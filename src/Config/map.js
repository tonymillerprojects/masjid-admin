import React from "react";
import ReactDOM from "react-dom";
// import { compose, withProps } from "recompose";
// import {
//   withScriptjs,
//   withGoogleMap,
//   GoogleMap,
//   Marker,
// } from "react-google-maps";

// const MyMapComponent = compose(
//   withProps({
//     googleMapURL:
//       "https://maps.google.com/maps/api/js?key=AIzaSyC2IS9uCoRhPn_t7Gz9yiIpb13KjCuMHLk&sensor=false&libraries=places",
//     loadingElement: <div style={{ height: `100%` }} />,
//     containerElement: <div style={{ height: `400px` }} />,
//     mapElement: <div style={{ height: `100%` }} />,
//   }),
//   withScriptjs,
//   withGoogleMap
// )((props) => (
//   <GoogleMap defaultZoom={8} defaultCenter={{ lat: -34.397, lng: 150.644 }}>  <SearchBox
//   ref={props.onSearchBoxMounted}
//   bounds={props.bounds}
//   controlPosition={google.maps.ControlPosition.TOP_LEFT}
//   onPlacesChanged={props.onPlacesChanged}
// >
//   <input
//     type="text"
//     placeholder="Customized your placeholder"
//     style={{
//       boxSizing: `border-box`,
//       border: `1px solid transparent`,
//       width: `240px`,
//       height: `32px`,
//       marginTop: `27px`,
//       padding: `0 12px`,
//       borderRadius: `3px`,
//       boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
//       fontSize: `14px`,
//       outline: `none`,
//       textOverflow: `ellipses`,
//     }}
//   />
// </SearchBox>
//     {props.isMarkerShown && (
//       <Marker
//         position={{ lat: -34.397, lng: 150.644 }}
//         onClick={props.onMarkerClick}
//       />
//     )}
//   </GoogleMap>
// ));

const _ = require("lodash");
const { compose, withProps, lifecycle } = require("recompose");
const {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} = require("react-google-maps");
const {
  SearchBox,
} = require("react-google-maps/lib/components/places/SearchBox");

const MapWithASearchBox = compose(
  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyC2IS9uCoRhPn_t7Gz9yiIpb13KjCuMHLk&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  lifecycle({
    componentWillMount() {
      console.log(this.props);
      const refs = {};
      this.setState({
        bounds: null,
        center: {
          lat: 41.9,
          lng: -87.624,
        },
        markers: [],
        onMapMounted: (ref) => {
          refs.map = ref;
        },
        onBoundsChanged: () => {
          this.setState({
            bounds: refs.map.getBounds(),
            center: refs.map.getCenter(),
          });
        },
        onSearchBoxMounted: (ref) => {
          refs.searchBox = ref;
        },
        onPlacesChanged: () => {
          const places = refs.searchBox.getPlaces();
          // eslint-disable-next-line no-undef
          const bounds = new google.maps.LatLngBounds();

          places.forEach((place) => {
            console.log(place);
            if (this.props.setLat || this.props.setLong) {
              this.props.setLat(place.geometry.location.lat());
              this.props.setLong(place.geometry.location.lng());
            }
            this.props.setLocation(place.formatted_address);
            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          const nextMarkers = places.map((place) => ({
            position: place.geometry.location,
          }));
          const nextCenter = _.get(
            nextMarkers,
            "0.position",
            this.state.center
          );

          this.setState({
            center: nextCenter,
            markers: nextMarkers,
          });
          // refs.map.fitBounds(bounds);
        },
      });
    },
  }),
  withScriptjs,
  withGoogleMap
)((props) => (
  <GoogleMap
    ref={props.onMapMounted}
    defaultZoom={15}
    center={props.center}
    onBoundsChanged={props.onBoundsChanged}
  >
    <SearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      // eslint-disable-next-line no-undef
      controlPosition={google.maps.ControlPosition.TOP_LEFT}
      onPlacesChanged={props.onPlacesChanged}
      style={{ width: 200 }}
    >
      <input
        type="text"
        placeholder="Location"
        style={{
          boxSizing: `border-box`,
          border: `1px solid transparent`,
          width: 200,
          height: `32px`,
          marginTop: `27px`,
          padding: `0 12px`,
          borderRadius: `3px`,
          boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
          fontSize: `14px`,
          outline: `none`,
          textOverflow: `ellipses`,
        }}
      />
    </SearchBox>
    {props.markers.map((marker, index) => (
      <Marker key={index} position={marker.position} />
    ))}
  </GoogleMap>
));
export default MapWithASearchBox;
