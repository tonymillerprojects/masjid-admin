import React from "react";
import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyAIOyANI636UFL9A2ul3buu8QG3lNE1LB4",
  authDomain: "masjid-f3de6.firebaseapp.com",
  databaseURL: "https://masjid-f3de6.firebaseio.com",
  projectId: "masjid-f3de6",
  storageBucket: "masjid-f3de6.appspot.com",
  messagingSenderId: "1070901058566",
  appId: "1:1070901058566:web:11f3e9b016e9f7759bf787",
  measurementId: "G-9P8SZ9L8Q5",
};
firebase.initializeApp(firebaseConfig);
const db = firebase.database();
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export const storage = firebase.storage();
class Firebase extends React.Component {
  render = () => {
    const childrenWithProps = React.Children.map(this.props.children, (child) =>
      React.cloneElement(child, { db })
    );
    return <div>{childrenWithProps}</div>;
  };
}

export default Firebase;
