import React from "react";
import Dialog from "@material-ui/core/Dialog";
import { makeStyles } from "@material-ui/core/styles";
import DialogContent from "@material-ui/core/DialogContent";
import { DialogActions, Button } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
  dialogPaper: {
    background: "#032b03",
    color: "white",
  },
}));
export default function ConfirmDialog(props) {
  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={props.openArchive}
        PaperProps={{ classes: { root: classes.dialogPaper } }}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>You are Archive this Masjid?</DialogContent>
        <DialogActions>
          <Button
            // variant="outlined"
            className="sv-btn1"
            style={{ color: "white" }}
            onClick={(e) => {
              e.preventDefault();
              props.handleMasjid(false);
            }}
          >
            Yes
          </Button>
          <Button
            // variant="outlined"
            className="sv-btn1"
            style={{ color: "white" }}
            onClick={(e) => {
              e.preventDefault();
              props.setOpenArchive(false);
            }}
          >
            No
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
