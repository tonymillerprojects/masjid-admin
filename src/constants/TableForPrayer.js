import React from "react";
import TimeKeeper from "react-timekeeper";
export default function TableForPrayers(props) {
  return (
    <div class="row">
      <div class="col-sm-12">
        <table
          id="inventory-table"
          class="table dataTable Admin-footer"
          style={{ width: "100%", borderSpacing: "0px" }}
          role="grid"
          aria-describedby="inventory-table_info"
        >
          <thead>
            <tr role="row" class="noBorder">
              <th
                class="sorting_asc"
                tabindex="0"
                aria-controls="inventory-table"
                rowspan="1"
                colspan="1"
                aria-sort="ascending"
                aria-label="PRODUCT NAME: activate to sort column descending"
                style={{ width: "40%" }}
              >
                <label> Prayers</label>
              </th>
              <th
                class="sorting"
                tabindex="0"
                aria-controls="inventory-table"
                rowspan="1"
                colspan="1"
                aria-label="PRICE: activate to sort column ascending"
                style={{ width: "30%" }}
              >
                <label> Azaan Time</label>
              </th>
              <th
                class="sorting"
                tabindex="0"
                aria-controls="inventory-table"
                rowspan="1"
                colspan="1"
                aria-label="IN STOCK: activate to sort column ascending"
                style={{ width: "30%" }}
              >
                <label>Jamaat Time</label>
              </th>
            </tr>
          </thead>
          <tbody>
            {!props.morePrayers
              ? ""
              : props.morePrayers.map((data, index) => {
                  if (data.heding) {
                    return (
                      <tr role="row" class="noBorder">
                        <td colspan="3">
                          <h1
                            class="text-center"
                            style={{ textAlign: "center" }}
                          >
                            {data.heding}
                          </h1>
                        </td>
                      </tr>
                    );
                  } else {
                    return (
                      <tr role="row" class="noBorder">
                        <td>
                          <label>{data.name}</label>
                        </td>
                        <td>
                          {props.rowIndex === index && props.hint === "azaa" ? (
                            props.showTime && (
                              <TimeKeeper
                                style={{ width: 240 }}
                                value={data.azaanTime}
                                onChange={(newTime) =>
                                  props.handleAddAzaanTime(
                                    newTime.formatted12,
                                    props.rowIndex
                                  )
                                }
                                onDoneClick={() => {
                                  props.setShowTime(false);
                                  props.setHint("");
                                  props.setRowIndex();
                                }}
                                switchToMinuteOnHourSelect
                              />
                            )
                          ) : (
                            <input
                              type="text"
                              onClick={() => {
                                props.setShowTime(true);
                                props.setHint("azaa");
                                props.setRowIndex(index);
                              }}
                              value={data.azaanTime}
                            ></input>
                          )}
                        </td>
                        <td>
                          {props.rowIndex === index && props.hint === "jama" ? (
                            props.showTime && (
                              <TimeKeeper
                                value={data.jamaatTime}
                                onChange={(newTime) =>
                                  props.handleAddJamaatTime(
                                    newTime.formatted12,
                                    props.rowIndex
                                  )
                                }
                                onDoneClick={() => {
                                  props.setShowTime(false);
                                  props.setHint("");
                                  props.setRowIndex();
                                }}
                                switchToMinuteOnHourSelect
                              />
                            )
                          ) : (
                            <input
                              type="text"
                              onClick={() => {
                                props.setShowTime(true);
                                props.setHint("jama");
                                props.setRowIndex(index);
                              }}
                              value={data.jamaatTime}
                            ></input>
                          )}
                        </td>
                      </tr>
                    );
                  }
                })}
          </tbody>
        </table>
      </div>
    </div>
  );
}
