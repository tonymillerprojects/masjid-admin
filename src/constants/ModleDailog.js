import React from "react";
import Loader from "react-loader-spinner";
import Dialog from "@material-ui/core/Dialog";
import { makeStyles } from "@material-ui/core/styles";
import DialogContent from "@material-ui/core/DialogContent";
const useStyles = makeStyles((theme) => ({
  dialogPaper: {
    borderRadius: 50,
    background: "#032b03",
  },
}));
export default function LoaderComponent(props) {
  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={props.open}
        PaperProps={{ classes: { root: classes.dialogPaper } }}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent
          style={{ padding: 0, paddingTop: 7, paddingRight: 7, paddingLeft: 7 }}
        >
          <Loader type="Puff" color="#006600" height={50} width={50} />
        </DialogContent>
      </Dialog>
    </div>
  );
}
