import React from "react";
export const prayerData = [
  { name: "FAJR", azaanTime: "", jamaatTime: "" },
  { name: "ZUHR", azaanTime: "", jamaatTime: "" },
  { name: "ASR", azaanTime: "", jamaatTime: "" },
  { name: "MAGHRIB", azaanTime: "", jamaatTime: "" },
  { name: "ISHA", azaanTime: "", jamaatTime: "" },
  { name: "JUMMNAH", azaanTime: "", jamaatTime: "" },
  { heding: "Sunday & Public Holidays" },
  { name: "ZUHR", azaanTime: "", jamaatTime: "" },
  { heding: "Next Salaah" },
  { name: "ISHA", azaanTime: "", jamaatTime: "" },
];
