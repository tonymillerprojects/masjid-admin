import React from "react";
import Masjid from "../View/Masjid";
import { storage } from "../Config/firebase";
import ConfirmDialog from "../constants/ConfirmDailog";
export default function MasjidContainer(props) {
  const [allMasjid, setAllMasjid] = React.useState([]);
  const [allMasjidId, setAllMasjidId] = React.useState();
  const [open, setOpen] = React.useState(false);
  const [openArchive, setOpenArchive] = React.useState(false);
  const [name, setName] = React.useState();
  const [lat, setLat] = React.useState();
  const [long, setLong] = React.useState();
  const [imageUrl, setImageUrl] = React.useState();
  const [imageName, setImageName] = React.useState();
  const [location, setLocation] = React.useState();
  const handleSaveMasjid = () => {
    const db = props.firebasedb;
    if (props.users.defaultMasjid) {
      alert("Remove your previous Masjid first then add new Masjid");
    } else {
      if (name && lat && long && imageUrl && location) {
        setOpen(true);
        var newPostKey = db.ref().child("masjid").push().key;
        // console.log(newPostKey);
        db.ref("masjid/" + newPostKey)
          .set({
            name: name,
            lat,
            imageUrl,
            long,
            location,
            userId: props.users.uid,
            status: "Active",
          })
          .then(() => {
            db.ref("users/" + props.users.uid)
              .update({
                defaultMasjidName: name,
                defaultMasjid: newPostKey,
              })
              .then(() => console.log("some "))
              .catch((e) => console.log(e));
            setImageUrl("");
            alert("Success");
            setLat("");
            setLong("");
            setLocation("");
            setName("");
            setOpen(false);
          })
          .catch((error) => {
            setOpen(false);
            alert(error);
          });
      } else {
        alert("All Field required !!");
      }
    }
  };
  React.useEffect(() => {
    const db = props.firebasedb;
    const masjidCollection = db
      .ref("masjid")
      .orderByChild("status")
      .equalTo("Active");
    setOpen(true);
    // console.log(masjidCollection);
    masjidCollection.on("value", async (snapshot) => {
      let allMasjid = [];
      await snapshot.forEach(function (data) {
        const customData = data.val();
        customData.key = data.key;
        if (data.key === props.users.defaultMasjid) {
          allMasjid.push(customData);
        }
      });
      setAllMasjid(allMasjid);
      setOpen(false);
    });
  }, [props.firebasedb, props.users.defaultMasjid]);
  const uploadImage = (event) => {
    const fireStorage = storage.ref();

    if (event.target.files[0]) {
      setOpen(true);
      const file = event.target.files[0];
      var FileSize = file.size / 1024 / 1024;
      if (FileSize <= 1) {
        setImageName(event.target.files[0].name);
        const image = fireStorage.child("images/" + event.target.files[0].name);
        image.put(file).then((snapshot) => {
          image.getDownloadURL().then((url) => {
            setImageUrl(url);
            setOpen(false);
          });
        });
      } else {
        alert("Max file size 1mb");
        setOpen(false);
      }
    }
  };
  const handleMasjid = () => {
    setOpenArchive(false);
    const db = props.firebasedb;
    db.ref("masjid/" + allMasjidId)
      .update({
        userId: props.users.uid,
        status: "Archive",
      })
      .then(() => alert("Success"))
      .catch((e) => console.log(e));
    db.ref("users/" + props.users.uid)
      .update({
        defaultMasjidName: "",
        defaultMasjid: "",
      })
      .then(() => console.log("Success"))
      .catch((e) => console.log(e));
  };
  return (
    <div>
      <Masjid
        setName={setName}
        users={props.users}
        open={open}
        imageName={imageName}
        imageUrl={imageUrl}
        setLat={setLat}
        allMasjid={allMasjid}
        setLong={setLong}
        handleMasjid={setOpenArchive}
        setLocation={setLocation}
        name={name}
        uploadImage={uploadImage}
        setAllMasjidId={setAllMasjidId}
        lat={lat}
        long={long}
        location={location}
        handleSaveMasjid={handleSaveMasjid}
      ></Masjid>
      <ConfirmDialog
        openArchive={openArchive}
        setOpenArchive={setOpenArchive}
        handleMasjid={handleMasjid}
      ></ConfirmDialog>
    </div>
  );
}
