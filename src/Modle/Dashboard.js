import React from "react";
import Dashboard from "../View/Dashboard";
export default function DashboardContainer(props) {
  return (
    <div>
      <Dashboard handleLogout={props.handleLogout}></Dashboard>
    </div>
  );
}
