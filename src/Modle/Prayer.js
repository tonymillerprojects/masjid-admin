import React from "react";
import { withRouter } from "react-router";
import { MASJId } from "../constants/routes";
import Prayer from "../View/Prayer";
import { prayerData } from "../constants/prayers";
import LoaderComponent from "../constants/ModleDailog";
function PrayerContainer(props) {
  const [allMasjid, setAllMasjid] = React.useState([]);
  const [allPrayers, setAllPrayers] = React.useState([]);
  const [prayerId, setPrayerId] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [name, setName] = React.useState();
  const [selectedMasjid, setSelectedMasjid] = React.useState();
  const [selectedMasjName, setSelectedMasjName] = React.useState();
  const [azaanTime, setAzaanTime] = React.useState();
  const [jamaatTime, setJamaatTime] = React.useState();
  const [morePrayers, setMorePrayers] = React.useState([]);
  const setSelectedMasjidAndName = (event) => {
    var sel = document.getElementById("masjidSelect");
    var opt = sel.options[sel.selectedIndex];
    setSelectedMasjid(opt.value);
    setSelectedMasjName(opt.text);
    handleGetPrayers(opt.value);
  };
  const handleEditPrayer = (
    key,
    name,
    azaanTime,
    jamaatTime,
    selectedMasjId,
    selectedMasjidName
  ) => {
    setName(name);
    setPrayerId(key);
    setSelectedMasjid(selectedMasjId);
    setAzaanTime(azaanTime);
    setJamaatTime(jamaatTime);
    setSelectedMasjName(selectedMasjidName);
  };
  const handleSavePrayer = () => {
    if (props.users.defaultMasjid) {
      if (morePrayers.length > 0) {
        setOpen(true);
        const db = props.firebasedb;
        if (prayerId) {
          db.ref("prayers/" + prayerId)
            .update({
              Prayers: morePrayers,
            })
            .then(() => {
              alert("Success");
              setName("");
              setSelectedMasjid("");
              setSelectedMasjName("");
              setAzaanTime("");
              setJamaatTime("");
              setOpen(false);
              setMorePrayers(allPrayers[0].Prayers);
            })
            .catch((error) => {
              setOpen(false);
              alert(error);
            });
        } else {
          var newPostKey = db.ref().child("prayers").push().key;
          db.ref("prayers/" + newPostKey)
            .set({
              Prayers: morePrayers,
              defaultMasjid: selectedMasjid,
              defaultMasjidName: selectedMasjName,
            })
            .then(() => {
              // setMorePrayers(allPrayers[0].Prayers);
              setName("");
              setSelectedMasjid("");
              setSelectedMasjName("");
              setAzaanTime("");
              setJamaatTime("");
              setOpen(false);
              alert("Success");
            })
            .catch((error) => {
              setOpen(false);
              alert(error);
            });
        }
      } else {
        alert("All Field Required !!");
      }
    } else {
      alert("You have don't belong to any Masjid");
      props.history.push(MASJId);
    }
    // });
  };
  const handleGetPrayers = (defaultMasjid) => {
    const db = props.firebasedb;
    const dataForPrayer = prayerData;
    const prayers = db.ref("prayers");
    prayers.orderByValue().on("value", async (snapshot) => {
      let allPrayers = [];
      // console.log(snapshot);
      await snapshot.forEach(function (data) {
        const customData = data.val();
        customData.key = data.key;
        if (customData.defaultMasjid === defaultMasjid) {
          allPrayers.push(customData);
          // console.log("if condi" + customData);
        } else {
          // console.log("else condi" + customData);
        }
      });
      if (allPrayers.length > 0) {
        setMorePrayers(allPrayers[0].Prayers);
        setPrayerId(allPrayers[0].key);
      } else {
        setMorePrayers(dataForPrayer);
        setPrayerId("");
      }
      setAllPrayers(allPrayers);
      setOpen(false);
    });
  };
  React.useEffect(() => {
    const db = props.firebasedb;
    setSelectedMasjName(props.users.defaultMasjidName);
    setSelectedMasjid(props.users.defaultMasjid);
    const masjidCollection = db.ref("masjid");
    setOpen(true);
    handleGetPrayers(props.users.defaultMasjid);
    masjidCollection.orderByValue().on("value", async (snapshot) => {
      let allMasjid = [];
      // console.log(snapshot);
      await snapshot.forEach(function (data) {
        const customData = data.val();
        customData.key = data.key;
        allMasjid.push(customData);
      });
      setAllMasjid(allMasjid);
      setOpen(false);
    });
  }, [props.firebasedb]);
  const handleAddJamaatTime = (value, index) => {
    morePrayers[index].jamaatTime = value;
    setMorePrayers(morePrayers);
  };
  const handleAddAzaanTime = (value, index) => {
    morePrayers[index].azaanTime = value;
    setMorePrayers(morePrayers);
  };
  // console.log(props);
  return (
    <div>
      <Prayer
        setName={setName}
        users={props.users}
        selectedMasjid={selectedMasjid}
        name={name}
        allMasjid={allMasjid}
        handleAddJamaatTime={handleAddJamaatTime}
        handleAddAzaanTime={handleAddAzaanTime}
        handleEditPrayer={handleEditPrayer}
        allPrayers={allPrayers}
        morePrayers={morePrayers}
        setSelectedMasjid={setSelectedMasjidAndName}
        setAzaanTime={setAzaanTime}
        azaanTime={azaanTime}
        setJamaatTime={setJamaatTime}
        jamaatTime={jamaatTime}
        handleSavePrayer={handleSavePrayer}
      ></Prayer>
      <LoaderComponent open={open}></LoaderComponent>
    </div>
  );
}
export default withRouter(PrayerContainer);
