import React from "react";
import Events from "../View/Events";
import { storage } from "../Config/firebase";
export default function EventsContainer(props) {
  const [open, setOpen] = React.useState(false);
  const [allMasjid, setAllMasjid] = React.useState([]);
  const [selectedMasjid, setSelectedMasjid] = React.useState();
  const [selectedMasjName, setSelectedMasjName] = React.useState();
  const [name, setName] = React.useState();
  const [eventDate, setEventDate] = React.useState();
  const [eventTime, setEventTime] = React.useState();
  const [location, setLocation] = React.useState();
  const [isMarkerShown, setIsMarkerShown] = React.useState(true);
  const [imageUrl, setImageUrl] = React.useState();
  const [description, setDescription] = React.useState();
  const uploadImage = (event) => {
    const fireStorage = storage.ref();

    if (event.target.files[0]) {
      setOpen(true);
      const file = event.target.files[0];
      var FileSize = file.size / 1024 / 1024;
      if (FileSize <= 1) {
        const image = fireStorage.child("images/" + event.target.files[0].name);
        image.put(file).then((snapshot) => {
          image.getDownloadURL().then((url) => {
            setImageUrl(url);
            setOpen(false);
          });
        });
      } else {
        alert("Max file size 1mb");
        setOpen(false);
      }
    }
  };
  const setSelectedMasjidAndName = (event) => {
    var sel = document.getElementById("masjidSelect");
    var opt = sel.options[sel.selectedIndex];
    setSelectedMasjid(opt.value);
    setSelectedMasjName(opt.text);
  };
  const handleSaveEvent = (e) => {
    const db = props.firebasedb;
    if (name && eventDate && eventTime && location) {
      setOpen(true);
      var newPostKey = db.ref().child("events").push().key;
      // console.log(newPostKey);
      db.ref("events/" + newPostKey)
        .set({
          name,
          eventDate,
          eventTime,
          imageUrl,
          selectedMasjid,
          selectedMasjidName: selectedMasjName,
          location,
          userId: props.users.uid,
          description,
        })
        .then(() => {
          alert("Success");
          setImageUrl("");
          setName("");
          setLocation("");
          setDescription("");
          setEventDate("");
          setEventTime("");
          setOpen(false);
        })
        .catch((error) => {
          setOpen(false);
          alert(error);
        });
    } else {
      alert("All Field Required");
    }
  };
  React.useEffect(() => {
    const db = props.firebasedb;
    const scoresRef = db.ref("masjid");
    scoresRef.orderByValue().on("value", async (snapshot) => {
      let allMasjid = [];
      await snapshot.forEach(function (data) {
        const customData = data.val();
        customData.key = data.key;
        allMasjid.push(customData);
        // console.log("The " + data.key + " dinosaur's score is " + data.val());
      });
      setAllMasjid(allMasjid);
    });
  }, [props.firebasedb]);

  const handleMarkerClick = () => {
    setIsMarkerShown(false);
    setTimeout(() => {
      setIsMarkerShown(true);
    }, 3000);
  };
  return (
    <div>
      <Events
        imageUrl={imageUrl}
        name={name}
        open={open}
        eventDate={eventDate}
        eventTime={eventTime}
        isMarkerShown={isMarkerShown}
        location={location}
        selectedMasjid={selectedMasjid}
        setName={setName}
        allMasjid={allMasjid}
        handleMarkerClick={handleMarkerClick}
        setSelectedMasjid={setSelectedMasjidAndName}
        uploadImage={uploadImage}
        setLocation={setLocation}
        setEventTime={setEventTime}
        description={description}
        setDescription={setDescription}
        setEventDate={setEventDate}
        handleSaveEvent={handleSaveEvent}
      ></Events>
    </div>
  );
}
