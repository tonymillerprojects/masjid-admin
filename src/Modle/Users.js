import React from "react";
import User from "../View/Users";
export default function UserContainer(props) {
  const [name, setName] = React.useState();
  const [email, setEmail] = React.useState();
  const [cellNumber, setCellNumber] = React.useState();
  const [role, setRole] = React.useState("Admin");
  const submitUsesData = () => {
    console.log(props);
    console.log("name=" + name);
    console.log("Email=" + email);
    console.log("Cell=" + cellNumber);
    console.log("role=" + role);
  };
  return (
    <div>
      <User
        name={name}
        email={email}
        cellNumber={cellNumber}
        role={role}
        setName={setName}
        setEmail={setEmail}
        setCellNumber={setCellNumber}
        setRole={setRole}
        submitUsesData={submitUsesData}
      ></User>
    </div>
  );
}
