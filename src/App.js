import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import LoaderComponent from "./constants/ModleDailog";
import {
  DASHBOARD,
  LOGIN,
  PRAYER,
  MASJId,
  BROADCOST,
  EVENTS,
} from "./constants/routes";
import Login from "./View/Login";
import MasjidContainer from "./Modle/Masjid";
import PrayerContainer from "./Modle/Prayer";
import BroadcastContainer from "./Modle/Broadcast";
import EventsContainer from "./Modle/Events";
import DashboardContainer from "./Modle/Dashboard";
import Layout from "./ThemeLayout/Layout";
import { auth } from "./Config/firebase";
function App(props) {
  const [users, setUsers] = React.useState([]);
  const [email, setEmail] = React.useState();
  const [open, setOpen] = React.useState(false);
  const [password, setPassword] = React.useState();
  const handleLogin = () => {
    const useData = props.db.ref("users");
    let userRole = null;
    let masjidId = null;
    let masjidName = null;
    let userName = null;
    setOpen(true);
    auth
      .signInWithEmailAndPassword(email, password)
      .then(async (data) => {
        await useData.orderByValue().on("value", async (snapshot) => {
          await snapshot.forEach(function (userList) {
            const customuserList = userList.val();
            customuserList.key = userList.key;
            if (customuserList.uid) {
              if (customuserList.uid === data.user.uid) {
                userRole = customuserList.role;
                masjidId = customuserList.defaultMasjid;
                masjidName = customuserList.defaultMasjidName;
                userName = customuserList.name;
              }
            }
          });
          let user = {
            uid: data.user.uid,
            email: data.user.email,
            displayName: userName,
            role: userRole,
            defaultMasjid: masjidId,
            defaultMasjidName: masjidName,
          };
          localStorage.setItem("user", JSON.stringify(user));
          setUsers(user);
          setOpen(false);
        });
      })
      .catch(function (error) {
        alert(error.message);
        setOpen(false);
      });
  };
  const handleLogout = () => {
    auth
      .signOut()
      .then(function () {
        setUsers([]);
        localStorage.setItem("user", false);
      })
      .catch(function (error) {
        // An error happened.
      });
  };
  React.useEffect(() => {
    setOpen(true);
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      const useData = props.db.ref("users/" + user.uid);
      useData.on("value", async (snapshot) => {
        console.log(snapshot.val());
        localStorage.setItem("user", JSON.stringify(snapshot.val()));
        setUsers(snapshot.val());
        setOpen(false);
      });
    } else {
      setOpen(false);
    }
  }, [props.db]);
  const firebasedb = props.db;
  return (
    <div className="App">
      {!users ? (
        ""
      ) : users.role === "user" ? (
        (alert("Permission denied"), setUsers(false))
      ) : users.uid !== undefined ? (
        <Router>
          <Layout users={users} handleLogout={handleLogout}>
            {users.role === "Mulana" ? (
              <Redirect exact from="/" to={MASJId} />
            ) : (
              <Redirect exact from="/" to={DASHBOARD} />
            )}
            {/* <Route
          path={USERS}
          render={(props) => (
            <UserContainer props={props} firebasedb={firebasedb} />
          )}
        /> */}
            <Route
              exact
              path={MASJId}
              render={(props) => (
                <MasjidContainer
                  {...props}
                  firebasedb={firebasedb}
                  users={users}
                />
              )}
            />
            <Route
              exact
              path={PRAYER}
              render={(props) => (
                <PrayerContainer
                  {...props}
                  firebasedb={firebasedb}
                  users={users}
                />
              )}
            />
            <Route
              exact
              path={BROADCOST}
              render={(props) => (
                <BroadcastContainer
                  {...props}
                  firebasedb={firebasedb}
                  users={users}
                />
              )}
            />
            <Route
              exact
              path={EVENTS}
              render={(props) => (
                <EventsContainer
                  {...props}
                  firebasedb={firebasedb}
                  users={users}
                />
              )}
            />
            <Route
              exact
              path={DASHBOARD}
              render={(props) => (
                <DashboardContainer
                  {...props}
                  firebasedb={firebasedb}
                  users={users}
                />
              )}
            />
          </Layout>
        </Router>
      ) : (
        <Router>
          <Redirect exact from="/" to={LOGIN} />
          <Route
            exact
            path={LOGIN}
            render={(props) => (
              <Login
                handleLogin={handleLogin}
                email={email}
                setEmail={setEmail}
                password={password}
                setPassword={setPassword}
              />
            )}
          />
        </Router>
      )}
      <LoaderComponent open={open}></LoaderComponent>
    </div>
  );
}
export default App;
