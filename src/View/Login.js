import React from "react";
import "../login.css";
export default function Login(props) {
  return (
    <div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <div
          style={{
            marginTop: 200,
            boxShadow: " 0px 0px 1px 1px #006600",
            width: 400,
          }}
        >
          <form>
            {/* <div class="imgcontainer">
              <img src="img_avatar2.png" alt="Avatar" class="avatar" />
            </div> */}

            <div class="container">
              <label for="uname">
                <b>Email ID</b>
              </label>
              <input
                type="text"
                placeholder="Enter Email"
                name="uname"
                required
                value={props.email}
                onChange={(e) => {
                  e.preventDefault();
                  props.setEmail(e.target.value);
                }}
              />
              <label for="psw">
                <b>Password</b>
              </label>
              <input
                type="password"
                placeholder="Enter Password"
                name="psw"
                required
                value={props.password}
                onChange={(e) => {
                  e.preventDefault();
                  props.setPassword(e.target.value);
                }}
              />
              <div style={{ display: "flex", justifyContent: "center" }}>
                <button
                  type="submit"
                  class="sv-btn"
                  onClick={(e) => {
                    e.preventDefault();
                    props.handleLogin();
                  }}
                >
                  Login
                </button>
              </div>
              {/* <label>
                <input type="checkbox" checked="checked" name="remember" />{" "}
                Remember me
              </label> */}
            </div>

            {/* <div class="container" style={{ backgroundColor: "#f1f1f1" }}>
              <button type="button" class="cancelbtn">
                Cancel
              </button>
              <span class="psw">
                Forgot <a href="#">password?</a>
              </span>
            </div> */}
          </form>
        </div>
      </div>
    </div>
  );
}
