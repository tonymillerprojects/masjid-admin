import React from "react";
// import ListView from "./ListView";
import TableForPrayers from "../constants/TableForPrayer";
// import TimeKeeper from "react-timekeeper";
export default function Prayer(props) {
  const [showTime, setShowTime] = React.useState(false);
  const [rowIndex, setRowIndex] = React.useState(false);
  const [hint, setHint] = React.useState();
  return (
    <div>
      <div class="content">
        {/* <!--START PAGE HEADER --> */}
        <header class="page-header">
          <div class="d-flex align-items-center"></div>
        </header>

        <section class="page-content container-fluid">
          <div class="row">
            <div class="col-lg-12 cstm-form">
              {!props.users ? (
                ""
              ) : !props.users.role ? (
                ""
              ) : props.users.role !== "admin" ? (
                ""
              ) : (
                <div class="col-md-4 form-group">
                  <label for="inputLocation">Masjid</label>
                  <select
                    class="form-control"
                    id="masjidSelect"
                    value={props.selectedMasjid}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setSelectedMasjid(e);
                    }}
                  >
                    <option value={""} name={""}>
                      Select
                    </option>
                    {!props.allMasjid
                      ? ""
                      : props.allMasjid.map((data, index) => (
                          <option
                            id={data.name}
                            value={data.key}
                            name={data.name}
                          >
                            {data.name}
                          </option>
                        ))}
                  </select>
                </div>
              )}

              <form>
                {/* <!-- work sections --> */}
                <h1 class="text-center" style={{ textAlign: "center" }}>
                  Masjid Salaah Times
                </h1>
                <br />
                <TableForPrayers
                  morePrayers={props.morePrayers}
                  setShowTime={setShowTime}
                  setHint={setHint}
                  rowIndex={rowIndex}
                  hint={hint}
                  showTime={showTime}
                  setRowIndex={setRowIndex}
                  handleAddAzaanTime={props.handleAddAzaanTime}
                  handleAddJamaatTime={props.handleAddJamaatTime}
                ></TableForPrayers>
                {/* <div class="row">
                  <div class="col-md-4 col-sm-4">
                    <label>Prayers </label>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <label>Azaan Time</label>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <label>Jamaat Time</label>
                  </div>
                </div>
                <br />
                {!props.morePrayers
                  ? ""
                  : props.morePrayers.map((data, index) => {
                      if (data.heding) {
                        return (
                          <div key={index}>
                            <br />
                            <div class="row">
                              <div class="col-md-12">
                                <h1
                                  class="text-center"
                                  style={{ textAlign: "center" }}
                                >
                                  {data.heding}
                                </h1>
                              </div>
                            </div>
                            <br />
                          </div>
                        );
                      } else {
                        return (
                          <div class="row" key={index}>
                            <div class="col-md-4">
                              <label>{data.name}</label>
                            </div>
                            <div class="col-md-4">
                              {rowIndex === index && hint === "azaa" ? (
                                showTime && (
                                  <TimeKeeper
                                    style={{ width: 240 }}
                                    value={data.azaanTime}
                                    onChange={(newTime) =>
                                      props.handleAddAzaanTime(
                                        newTime.formatted12,
                                        rowIndex
                                      )
                                    }
                                    onDoneClick={() => {
                                      setShowTime(false);
                                      setHint("");
                                      setRowIndex();
                                    }}
                                    switchToMinuteOnHourSelect
                                  />
                                )
                              ) : (
                                <input
                                  type="text"
                                  onClick={() => {
                                    setShowTime(true);
                                    setHint("azaa");
                                    setRowIndex(index);
                                  }}
                                  value={data.azaanTime}
                                ></input>
                              )}
                            </div>
                            <div class="col-md-4">
                              {rowIndex === index && hint === "jama" ? (
                                showTime && (
                                  <TimeKeeper
                                    value={data.jamaatTime}
                                    onChange={(newTime) =>
                                      props.handleAddJamaatTime(
                                        newTime.formatted12,
                                        rowIndex
                                      )
                                    }
                                    onDoneClick={() => {
                                      setShowTime(false);
                                      setHint("");
                                      setRowIndex();
                                    }}
                                    switchToMinuteOnHourSelect
                                  />
                                )
                              ) : (
                                <input
                                  type="text"
                                  onClick={() => {
                                    setShowTime(true);
                                    setHint("jama");
                                    setRowIndex(index);
                                  }}
                                  value={data.jamaatTime}
                                ></input>
                              )}
                            </div>
                          </div>
                        );
                      }
                    })} */}
                <br />
                <div class="row">
                  <div class="col-md-2" style={{ textAlign: "center" }}></div>
                  <div class="col-md-8" style={{ textAlign: "center" }}>
                    <button
                      class="sv-btn"
                      onClick={(e) => {
                        e.preventDefault();
                        props.handleSavePrayer(e);
                      }}
                    >
                      Save
                    </button>
                  </div>
                  <div class="col-md-2" style={{ textAlign: "center" }}></div>
                </div>
              </form>
            </div>
          </div>
          {/* <div class="row" style={{ marginTop: 10 }}>
            <div class="col-lg-12 cstm-form">
              <ListView
                allPrayers={props.allPrayers}
                handleEditPrayer={props.handleEditPrayer}
              ></ListView>
            </div>
          </div> */}
        </section>
        {/* <section class="page-content container-fluid ">
          <div class="row">
            <div class="col-lg-12 cstm-form">
              <ListView></ListView>
            </div>
          </div>
        </section> */}
      </div>
    </div>
  );
}
