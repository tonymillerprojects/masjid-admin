import React from "react";
import LoaderComponent from "../constants/ModleDailog";
import MyMapComponent from "../Config/map";
import ListView from "./ListView";
export default function Masjid(props) {
  return (
    <div>
      <LoaderComponent open={props.open}></LoaderComponent>
      <div class="content">
        {/* <!--START PAGE HEADER --> */}
        <header class="page-header">
          <div class="d-flex align-items-center"></div>
        </header>
        {/* <!--END PAGE HEADER -->
               <!--START PAGE CONTENT --> */}
        <section class="page-content container">
          <div class="row">
            <div class="col-lg-12 cstm-form1" style={{ marginTop: -10 }}>
              {/* <!-- work sections --> */}
              <div class="row ">
                <div class="col-md-12">
                  <div class="mr-auto">
                    <h1>Add Masjid</h1>
                    {!props.imageUrl ? (
                      ""
                    ) : (
                      <img
                        width="90px"
                        height="68px"
                        src={props.imageUrl}
                      ></img>
                    )}
                  </div>
                  <br />
                  <label>Masjid Name</label>
                  <input
                    type="text"
                    placeholder="Masjid Name"
                    value={props.name}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setName(e.target.value);
                    }}
                  />
                  {/* <div class="col-md-12"> */}
                  <label>Masjid Image</label>
                  <div class="custom-file" style={{ marginBottom: 25 }}>
                    <input
                      type="file"
                      class="custom-file-input"
                      accept="image/*"
                      id="customFile"
                      onChange={(e) => {
                        e.preventDefault();
                        props.uploadImage(e);
                      }}
                    />
                    <label class="custom-file-label" for="customFile">
                      Choose file
                    </label>
                  </div>
                  {/* </div> */}
                  <label>Latitude</label>
                  <input
                    type="text"
                    placeholder="Latitude"
                    value={props.lat}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setLat(e.target.value);
                    }}
                  />
                  <label>Longitude</label>
                  <input
                    type="text"
                    placeholder="Longitude"
                    value={props.long}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setLong(e.target.value);
                    }}
                  />
                  <label>Location</label>
                  <input
                    type="text"
                    placeholder="Location"
                    value={props.location}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setLocation(e.target.value);
                    }}
                  />
                </div>
              </div>
              <MyMapComponent
                setLat={props.setLat}
                setLong={props.setLong}
                setLocation={props.setLocation}
              ></MyMapComponent>
              <form>
                <button
                  class="sv-btn1"
                  onClick={(e) => {
                    e.preventDefault();
                    props.handleSaveMasjid(e);
                  }}
                >
                  Save
                </button>
              </form>
            </div>
          </div>
          <div class="row" style={{ marginTop: 10 }}>
            <div class="col-lg-12 cstm-formList">
              <ListView
                tableFor="masjid"
                buttonLabel="Archive"
                allPrayers={props.allMasjid}
                setAllMasjidId={props.setAllMasjidId}
                handleMasjid={props.handleMasjid}
              ></ListView>
            </div>
          </div>
        </section>
        {/* <!--END PAGE CONTENT --> */}
      </div>
    </div>
  );
}
