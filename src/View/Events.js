import React from "react";
import LoaderComponent from "../constants/ModleDailog";
import MyMapComponent from "../Config/map";
export default function Events(props) {
  return (
    <div>
      <LoaderComponent open={props.open}></LoaderComponent>
      <div class="content">
        {/* <!--START PAGE HEADER --> */}
        <header class="page-header">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h1>Events</h1>
            </div>
          </div>
        </header>
        {/* <!--END PAGE HEADER -->
               <!--START PAGE CONTENT --> */}
        <section class="page-content container">
          <div class="row">
            <div class="col-lg-12 cstm-form-event" style={{ marginTop: -10 }}>
              <div className="row">
                <div class="col-md-4 form-group">
                  <label for="inputLocation">Masjid</label>
                  <select
                    class="form-control"
                    id="masjidSelect"
                    value={props.selectedMasjid}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setSelectedMasjid(e);
                    }}
                  >
                    <option value={""} name={""}>
                      Select
                    </option>
                    {!props.allMasjid
                      ? ""
                      : props.allMasjid.map((data, index) => (
                          <option
                            id={data.name}
                            value={data.key}
                            name={data.name}
                          >
                            {data.name}
                          </option>
                        ))}
                  </select>
                </div>
                <div class="col-md-4 form-group">
                  {!props.imageUrl ? (
                    ""
                  ) : (
                    <img width="90px" height="68px" src={props.imageUrl}></img>
                  )}
                </div>
              </div>
              <div class="row ">
                <div class="col-md-6">
                  <label>Event Name</label>
                  <input
                    type="text"
                    placeholder="Event Name"
                    value={props.name}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setName(e.target.value);
                    }}
                  />
                </div>
                <div class="col-md-6">
                  <label>Event Date</label>
                  <input
                    placeholder="Date"
                    class="textbox-n"
                    type="date"
                    onfocus="(this.type='date')"
                    id="date"
                    value={props.eventDate}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setEventDate(e.target.value);
                    }}
                  />
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label>Event Time</label>
                  <input
                    placeholder="Time"
                    class="textbox-n"
                    type="time"
                    onfocus="(this.type='time')"
                    id="time"
                    value={props.eventTime}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setEventTime(e.target.value);
                    }}
                  />
                </div>
                <div class="col-md-6">
                  <label>Event Image</label>
                  <div class="custom-file">
                    <input
                      type="file"
                      accept="image/*"
                      class="custom-file-input"
                      id="customFile"
                      onChange={(e) => {
                        e.preventDefault();
                        props.uploadImage(e);
                      }}
                    />
                    <label class="custom-file-label" for="customFile">
                      Choose file
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 ">
                  <label>Description</label>
                  <br />
                  <input
                    type="text"
                    placeholder="description"
                    value={props.description}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setDescription(e.target.value);
                    }}
                  />
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 ">
                  <label>Location</label>
                  <br />
                  <input
                    type="text"
                    placeholder="Location"
                    value={props.location}
                    onChange={(e) => {
                      e.preventDefault();
                      props.setLocation(e.target.value);
                    }}
                  />
                  <MyMapComponent
                    setLocation={props.setLocation}
                  ></MyMapComponent>
                  {/* <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27326461.428761277!2d9.096185190924585!3d-33.273044907593956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1c34a689d9ee1251%3A0xe85d630c1fa4e8a0!2sSouth%20Africa!5e0!3m2!1sen!2s!4v1582966080988!5m2!1sen!2s"
                    width="100%"
                    height="450"
                    frameborder="0"
                    style={{ border: 0 }}
                    allowfullscreen=""
                  ></iframe> */}
                  <br />
                  <form
                    onSubmit={(e) => {
                      e.preventDefault();
                      props.handleSaveEvent(e);
                    }}
                  >
                    <button type="submit" class="sv-btn-event">
                      Save
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <!--END PAGE CONTENT --> */}
      </div>
    </div>
  );
}
