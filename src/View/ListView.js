import React from "react";
export default function ListView(props) {
  return (
    <div class="card-body">
      <div class="table-responsive">
        <div
          id="inventory-table_wrapper"
          class="dataTables_wrapper container-fluid dt-bootstrap4 Admin-footer"
        >
          <div class="row">
            <div class="col-sm-12">
              <table
                id="inventory-table"
                class="table table-striped table-bordered dataTable Admin-footer"
                style={{ width: "100%" }}
                role="grid"
                aria-describedby="inventory-table_info"
              >
                <thead>
                  <tr role="row">
                    <th
                      class="sorting_asc"
                      tabindex="0"
                      aria-controls="inventory-table"
                      rowspan="1"
                      colspan="1"
                      aria-sort="ascending"
                      aria-label="PRODUCT NAME: activate to sort column descending"
                      style={{ width: 450 }}
                    >
                      Masjid Name
                    </th>
                    <th
                      class="sorting"
                      tabindex="0"
                      aria-controls="inventory-table"
                      rowspan="1"
                      colspan="1"
                      aria-label="PRICE: activate to sort column ascending"
                      style={{ width: 89 }}
                    >
                      Latitude
                    </th>
                    <th
                      class="sorting"
                      tabindex="0"
                      aria-controls="inventory-table"
                      rowspan="1"
                      colspan="1"
                      aria-label="IN STOCK: activate to sort column ascending"
                      style={{ width: 132 }}
                    >
                      Longitude
                    </th>
                    <th
                      class="sorting"
                      tabindex="0"
                      aria-controls="inventory-table"
                      rowspan="1"
                      colspan="1"
                      aria-label="TYPE: activate to sort column ascending"
                      style={{ width: 119 }}
                    >
                      Location
                    </th>
                    <th
                      class="sorting"
                      tabindex="0"
                      aria-controls="inventory-table"
                      rowspan="1"
                      colspan="1"
                      aria-label="TYPE: activate to sort column ascending"
                      style={{ width: 119 }}
                    >
                      Image
                    </th>
                    <th
                      class="sorting"
                      tabindex="0"
                      aria-controls="inventory-table"
                      rowspan="1"
                      colspan="1"
                      aria-label=": activate to sort column ascending"
                      style={{ width: 89 }}
                    ></th>
                  </tr>
                </thead>
                <tbody>
                  {props.allPrayers.map((data, index) => (
                    <tr role="row" class="odd" key={index}>
                      <td>{data.name}</td>
                      <td>{data.lat}</td>
                      <td>{data.long}</td>
                      <td>{data.location}</td>
                      <td>
                        <img src={data.imageUrl} alt="No"></img>
                      </td>
                      <td>
                        <button
                          class="btn btn-primary btn-outline btn-sm"
                          data-target="#editPanadoModal"
                          title="Edit Panado"
                          onClick={(e) => {
                            e.preventDefault();
                            props.handleMasjid(true);
                            props.setAllMasjidId(data.key);
                          }}
                        >
                          {props.buttonLabel}
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
