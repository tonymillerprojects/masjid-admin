import React from "react";
// import Layout from "../ThemeLayout/Layout";
export default function Dashboard(props) {
  return (
    <div>
      <div class="content">
        <header class="page-header">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h1>Dashboard</h1>
            </div>
            {/* <ul class="actions top-right">
              <li class="dropdown">
                <a
                  href="javascript:void(0)"
                  class="btn btn-fab"
                  data-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i class="la la-ellipsis-h"></i>
                </a>
                <div class="dropdown-menu dropdown-icon-menu dropdown-menu-right">
                  <div class="dropdown-header">Quick Actions</div>
                  <a href="index.html#" class="dropdown-item">
                    <i class="icon dripicons-clockwise"></i> Refresh
                  </a>
                  <a href="support.html" class="dropdown-item">
                    <i class="icon dripicons-help"></i> Support
                  </a>
                </div>
              </li>
            </ul> */}
          </div>
        </header>
        <section class="page-content container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="row m-0 col-border-xl">
                  <div class="col-md-12 col-lg-6 col-xl-3">
                    <div class="card-body">
                      <div class="icon-rounded icon-rounded-primary float-left m-r-20">
                        <i class="icon dripicons-graph-bar"></i>
                      </div>
                      <h5 class="card-title m-b-5 counter" data-count="56">
                        0
                      </h5>
                      <h6 class="text-muted m-t-10">Total Users</h6>
                      <small
                        class="text-muted float-right m-t-5 mb-3 counter append-percent"
                        data-count="72"
                      >
                        0
                      </small>
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-6 col-xl-3">
                    <div class="card-body">
                      <div class="icon-rounded icon-rounded-accent float-left m-r-20">
                        <i class="icon dripicons-cart"></i>
                      </div>
                      <h5
                        class="card-title m-b-5 append-percent counter"
                        data-count="67"
                      >
                        0
                      </h5>
                      <h6 class="text-muted m-t-10">Total Subscriptions</h6>
                      <div
                        class="progress progress-add-to-cart mt-4"
                        style={{ height: 7 }}
                      >
                        <div
                          class="progress-bar bg-accent"
                          role="progressbar"
                          aria-valuenow="67"
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                      <small
                        class="text-muted float-right m-t-5 mb-3 counter append-percent"
                        data-count="67"
                      >
                        0
                      </small>
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-6 col-xl-3">
                    <div class="card-body">
                      <div class="icon-rounded icon-rounded-info float-left m-r-20">
                        <i class="icon dripicons-tag"></i>
                      </div>
                      <h5 class="card-title m-b-5 counter" data-count="337">
                        0
                      </h5>
                      <h6 class="text-muted m-t-10">Broadcasts Archives</h6>
                      <div
                        class="progress progress-new-account mt-4"
                        style={{ height: 7 }}
                      >
                        <div
                          class="progress-bar bg-info"
                          role="progressbar"
                          aria-valuenow="83"
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                      <small class="text-muted float-left m-t-5 mb-3">
                        View Products
                      </small>
                      <small
                        class="text-muted float-right m-t-5 mb-3 counter append-percent"
                        data-count="83"
                      >
                        0
                      </small>
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-6 col-xl-3">
                    <div class="card-body">
                      <div class="icon-rounded icon-rounded-success float-left m-r-20">
                        <i class="la la-dollar f-w-600"></i>
                      </div>
                      <h5 class="card-title m-b-5 counter" data-count="122">
                        0
                      </h5>
                      <h6 class="text-muted m-t-10">Total Masjid Added</h6>
                      <div
                        class="progress progress-total-revenue mt-4"
                        style={{ height: 7 }}
                      >
                        <div
                          class="progress-bar bg-success"
                          role="progressbar"
                          aria-valuenow="77"
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                      <small class="text-muted float-left m-t-5 mb-3">
                        View Sales
                      </small>
                      <small
                        class="text-muted float-right m-t-5 mb-3 counter append-percent"
                        data-count="77"
                      >
                        0
                      </small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
