import React from "react";
import Layout from "../ThemeLayout/Layout";
export default function Users(props) {
  return (
    <Layout>
      <section className="section mainsection">
        <div class="content">
          {/* <!--START PAGE HEADER --> */}
          <header class="page-header">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h1>Manage Users</h1>
              </div>
            </div>
          </header>
          {/* <!--END PAGE HEADER -->
               <!--START PAGE CONTENT --> */}
          <section class="page-content container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <div class="row" style={{ float: "right " }}>
                      <button
                        type="button"
                        class="btn btn-primary"
                        data-toggle="modal"
                        data-target="#addProductModal"
                        title="Add Product"
                        style={{ marginRight: 15 }}
                      >
                        Add User
                      </button>
                      <div
                        class="modal"
                        tabindex="-1"
                        role="dialog"
                        id="addProductModal"
                      >
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5
                                class="modal-title"
                                style={{ textAlign: "center" }}
                              >
                                Add Stores
                              </h5>
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label for="inputName">Full Name</label>
                                  <input
                                    type="text"
                                    class="form-control"
                                    id="inputName"
                                    value={props.name}
                                    autocomplete="name"
                                    onChange={(e) => {
                                      e.preventDefault();
                                      props.setName(e.target.value);
                                    }}
                                    placeholder="Enter your Name"
                                  />
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Email</label>
                                  <input
                                    type="email"
                                    class="form-control"
                                    autocomplete="email"
                                    value={props.email}
                                    placeholder="Enter your Email"
                                    onChange={(e) => {
                                      e.preventDefault();
                                      props.setEmail(e.target.value);
                                    }}
                                  />
                                </div>
                                <div class="form-group">
                                  <label for="inputName">Cell</label>
                                  <input
                                    type="number"
                                    class="form-control"
                                    id="inputName"
                                    value={props.cellNumber}
                                    onChange={(e) => {
                                      e.preventDefault();
                                      props.setCellNumber(e.target.value);
                                    }}
                                    autocomplete="name"
                                    placeholder="Enter your Cell No."
                                  />
                                </div>
                                <div class="form-group">
                                  <label for="inputLocation">Role</label>
                                  <select
                                    class="form-control"
                                    id="exampleFormControlSelect1"
                                    value={props.role}
                                    onChange={(e) => {
                                      e.preventDefault();
                                      props.setRole(e.target.value);
                                    }}
                                  >
                                    <option value="Admin">Admin</option>
                                    <option value="User">User</option>
                                  </select>
                                </div>
                              </form>
                            </div>
                            <div class="modal-footer">
                              <button
                                type="button"
                                class="btn btn-secondary"
                                data-dismiss="modal"
                                onClick={(e) => {
                                  e.preventDefault();
                                  props.setCellNumber("");
                                  props.setName("");
                                  props.setEmail("");
                                  props.setRole("");
                                }}
                              >
                                Close
                              </button>
                              <button
                                type="button"
                                class="btn btn-primary"
                                onClick={(e) => {
                                  e.preventDefault();
                                  props.submitUsesData(e);
                                }}
                              >
                                Save
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <div
                        id="inventory-table_wrapper"
                        class="dataTables_wrapper container-fluid dt-bootstrap4 Admin-footer"
                      >
                        <div class="row">
                          <div class="col-sm-12 col-md-6">
                            <div
                              class="dataTables_length"
                              id="inventory-table_length"
                            >
                              <label>
                                Show
                                <select
                                  name="inventory-table_length"
                                  aria-controls="inventory-table"
                                  class="form-control form-control-sm"
                                >
                                  <option value="10">10</option>
                                  <option value="25">25</option>
                                  <option value="50">50</option>
                                  <option value="100">100</option>
                                </select>
                                entries
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-6">
                            <div
                              id="inventory-table_filter"
                              class="dataTables_filter"
                            >
                              <label>
                                Search:
                                <input
                                  type="search"
                                  class="form-control form-control-sm"
                                  placeholder=""
                                  aria-controls="inventory-table"
                                />
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                            <table
                              id="inventory-table"
                              class="table table-striped table-bordered dataTable Admin-footer"
                              style={{ width: "100%" }}
                              role="grid"
                              aria-describedby="inventory-table_info"
                            >
                              <thead>
                                <tr role="row">
                                  <th
                                    class="sorting_asc"
                                    tabindex="0"
                                    aria-controls="inventory-table"
                                    rowspan="1"
                                    colspan="1"
                                    aria-sort="ascending"
                                    aria-label="PRODUCT NAME: activate to sort column descending"
                                    style={{ width: 550 }}
                                  >
                                    FUll Name
                                  </th>
                                  <th
                                    class="sorting"
                                    tabindex="0"
                                    aria-controls="inventory-table"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="PRICE: activate to sort column ascending"
                                    style={{ width: 89 }}
                                  >
                                    Email
                                  </th>
                                  <th
                                    class="sorting"
                                    tabindex="0"
                                    aria-controls="inventory-table"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="IN STOCK: activate to sort column ascending"
                                    style={{ width: 132 }}
                                  >
                                    Role
                                  </th>
                                  <th
                                    class="sorting"
                                    tabindex="0"
                                    aria-controls="inventory-table"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label="TYPE: activate to sort column ascending"
                                    style={{ width: 119 }}
                                  >
                                    Cell
                                  </th>
                                  <th
                                    class="sorting"
                                    tabindex="0"
                                    aria-controls="inventory-table"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label=": activate to sort column ascending"
                                    style={{ width: 89 }}
                                  ></th>
                                  <th
                                    class="sorting"
                                    tabindex="0"
                                    aria-controls="inventory-table"
                                    rowspan="1"
                                    colspan="1"
                                    aria-label=": activate to sort column ascending"
                                    style={{ width: 119 }}
                                  ></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr role="row" class="odd">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>User</td>
                                  <td>071 2322 232</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="even">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>User</td>
                                  <td>071 2322 232</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="odd">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>User</td>
                                  <td>071 2322 232</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="even">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>User</td>
                                  <td>071 2322 232</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="odd">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>Admin</td>
                                  <td>General</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="even">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>Admin</td>
                                  <td>General</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="odd">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>Admin</td>
                                  <td>General</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="even">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>User</td>
                                  <td>071 2322 232</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="odd">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>Admin</td>
                                  <td>General</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                                <tr role="row" class="even">
                                  <td class="sorting_1">
                                    <img
                                      class="align-self-center mr-3 ml-2 w-50 rounded-circle"
                                      src="assets/img/avatars/11.jpg"
                                      alt=""
                                    />
                                    <strong class="Adminwrap">
                                      Aaron Elliott
                                    </strong>
                                  </td>
                                  <td>email@domain.com</td>
                                  <td>Admin</td>
                                  <td>General</td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      data-toggle="modal"
                                      data-target="#editPanadoModal"
                                      title="Edit Panado"
                                    >
                                      Edit
                                    </button>
                                  </td>
                                  <td>
                                    <button
                                      class="btn btn-primary btn-outline btn-sm"
                                      onclick="productDelete()"
                                    >
                                      Delete
                                    </button>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-12 col-md-5">
                            <div
                              class="dataTables_info"
                              id="inventory-table_info"
                              role="status"
                              aria-live="polite"
                            >
                              Showing 1 to 10 of 10 entries
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-7">
                            <div
                              class="dataTables_paginate paging_simple_numbers"
                              id="inventory-table_paginate"
                            >
                              <ul class="pagination">
                                <li
                                  class="paginate_button page-item previous disabled"
                                  id="inventory-table_previous"
                                >
                                  Previous
                                </li>
                                <li class="paginate_button page-item active">
                                  1
                                </li>
                                <li
                                  class="paginate_button page-item next disabled"
                                  id="inventory-table_next"
                                >
                                  Next
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row"></div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </section>
    </Layout>
  );
}
