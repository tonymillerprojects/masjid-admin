const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp();

exports.notificationSent = functions.database
  .ref("/prayers/{key}")
  .onUpdate(async (change, context) => {
    // console.log(change);
    // console.log(change.after._data);
    const data = change.after._data;
    const masjId = change.after._data.defaultMasjid;
    // const previousData = change.before._data;
    const text = "Prayer Time Changed ";

    const payload = {
      notification: {
        title: `${data.defaultMasjidName}`,
        body: text
          ? text.length <= 100
            ? text
            : text.substring(0, 97) + "..."
          : "",
        icon: !data.profilePicUrl
          ? data.defaultMasjidName.slice(0, 1)
          : data.profilePicUrl,
      },
    };

    // Get the list of device tokens.
    const allTokens = await admin.database().ref("users");
    // let tokens = [];
    allTokens.orderByValue().on("value", async (snapshot) => {
      await snapshot.forEach(function (data) {
        const customData = data.val();
        if (customData.defaultMasjid) {
          if (customData.isPrayerNotification) {
            // console.log(customData.defaultMasjid === masjId);
            if (customData.defaultMasjid === masjId) {
              if (customData.notificationToken) {
                admin
                  .messaging()
                  .sendToDevice(customData.notificationToken, payload)
                  .then(function (response) {
                    console.log("Successfully sent message:", response);
                  })
                  .catch(function (error) {
                    console.log("Error sending message:", error);
                  });
              }
            }
          }
        }
      });
    });
    // } else {
    //   console.log("Some essue !!");
    // }
  });
exports.createBroadcost = functions.database
  .ref("broadcast/{key}")
  .onCreate(async (change, context) => {
    const newValue = change.val();
    const userId = change.val().userId;
    const text = "Broadcast";
    if (newValue) {
      const payload = {
        notification: {
          title: `${newValue.name}`,
          body: text
            ? text.length <= 100
              ? text
              : text.substring(0, 97) + "..."
            : "",
          icon: !newValue.profilePicUrl
            ? newValue.name.slice(0, 1)
            : newValue.profilePicUrl,
        },
      };
      const allTokens = await admin.database().ref("users");
      await allTokens.orderByValue().on("value", async (snapshot) => {
        snapshot.forEach(function (data) {
          const customData = data.val();
          if (customData.uid) {
            if (customData.uid === userId) {
              allTokens.orderByValue().on("value", async (snapshot) => {
                snapshot.forEach(function (data) {
                  const allData = data.val();
                  if (allData.defaultMasjid) {
                    if (allData.isBroadcastNotification) {
                      if (allData.defaultMasjid === customData.defaultMasjid) {
                        if (allData.uid !== userId) {
                          if (allData.notificationToken) {
                            admin
                              .messaging()
                              .sendToDevice(allData.notificationToken, payload)
                              .then(function (response) {
                                console.log(
                                  "Successfully sent message:",
                                  response
                                );
                              })
                              .catch(function (error) {
                                console.log("Error sending message:", error);
                              });
                          }
                        }
                      }
                    }
                  }
                });
              });
            }
          }
        });
      });
    }
    // perform desired operations ...
  });

exports.createEvents = functions.database
  .ref("events/{userId}")
  .onCreate(async (snap, context) => {
    const newValue = snap.val();
    const text =
      "Event Time At " + newValue.eventTime + " Date " + newValue.eventDate;
    if (newValue) {
      const payload = {
        notification: {
          title: `${newValue.name}" location " ${newValue.location}`,
          body: text
            ? text.length <= 100
              ? text
              : text.substring(0, 97) + "..."
            : "",
          icon: !newValue.profilePicUrl
            ? newValue.name.slice(0, 1)
            : newValue.profilePicUrl,
        },
      };

      // Get the list of device tokens.
      const allTokens = await admin.database().ref("users");
      await allTokens.orderByValue().on("value", (snapshot) => {
        snapshot.forEach(function (data) {
          const customData = data.val();
          if (customData.defaultMasjid) {
            if (customData.isEventNotification) {
              if (customData.defaultMasjid === newValue.selectedMasjid) {
                if (customData.uid !== newValue.userId) {
                  if (customData.notificationToken) {
                    admin
                      .messaging()
                      .sendToDevice(customData.notificationToken, payload)
                      .then(function (response) {
                        console.log("Successfully sent message:", response);
                      })
                      .catch(function (error) {
                        console.log("Error sending message:", error);
                      });
                  }
                }
              }
            }
          }
        });
      });
    }
    // perform desired operations ...
  });
